const express = require('express');
const indexRouter = require('./routes');
const http = require('http');
const cookieParser = require('cookie-parser');

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(cookieParser());

app.set("view engine", "ejs");

app.use('/', indexRouter);

const server = http.createServer(app);
server.listen(process.env.PORT || 3000);